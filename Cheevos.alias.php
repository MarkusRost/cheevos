<?php
/**
 * Cheevos
 * Cheevos Aliases
 *
 * @author		Hydra Wiki Platform Team
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Cheevos
 * @link		https://gitlab.com/hydrawiki
 *
 **/

$specialPageAliases = [];

/**
 * English
 */
$specialPageAliases['en'] = [
	'Achievements'			=> ['Achievements'],
	'AchievementStats'		=> ['AchievementStats'],
	'AwardAchievement'		=> ['AwardAchievement'],
	'ManageAchievements'	=> ['ManageAchievements'],
	'MegaAchievements'		=> ['MegaAchievements'],
	'PointsComp'			=> ['PointsComp'],
	'WikiPoints'			=> ['WikiPoints'],
	'WikiPointsAdmin'		=> ['WikiPointsAdmin'],
	'WikiPointsLevels'		=> ['WikiPointsLevels'],
	'WikiPointsMultipliers'	=> ['WikiPointsMultipliers']
];
